# PostsToFiles
PostToFiles is a simple application that fetches data about posts from [{JSON} Placeholder
 API](https://jsonplaceholder.typicode.com/posts) and writes them into separate files with extension `json` in `/posts` directory.

## Run
To run the application open a terminal in the root directory `PostsToFiles` and then type `./gradlew run` or use IDE.

## Test
To run all tests open a terminal in the root directory `PostsToFiles` and then type `./gradlew test` or use IDE.

### Networking

#### [OkHttp](https://square.github.io/okhttp/) 
Library used due to the client functionality which enables fetching data from different external RESTful API services. Currently it is used in a synchronous way.

### Tests

#### [JUnit](https://junit.org/junit5/)
Test framework created especially for JVM languages like Java or Kotlin. The open-source framework was especially written to deal with automated tests.

#### [Mockito-kotlin](https://github.com/mockito/mockito-kotlin)
Open-source test framework that enables possibility to mock behavior of particular elements ex. objects, to extend  potential of unit testing of provided code.

### Insights
 - Things like `FILE_EXTENSION` and `POSTS_URL` could be provided by environment variables, but nothing like that was mentioned in the assignment
 - I decided about mapping of `JSON` response to list of objects due to improvement of the code readability, what is more, working with such structures is much easier than with plain `String/JSON`
 - In order to serialize or deserialize objects I used [`Kotlin serialization`](https://github.com/Kotlin/kotlinx.serialization) due the fact that it is completely reflectionless. This fact makes it really fast in comparison to different tools, ex. Jackson. 