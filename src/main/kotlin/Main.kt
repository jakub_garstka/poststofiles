import fetcher.PostsFetcher
import fetcher.PostsFetcherImpl
import kotlinx.coroutines.runBlocking
import writer.PostsWriter
import writer.PostsWriterImpl

fun main() = runBlocking {
    val postsFetcher: PostsFetcher = PostsFetcherImpl()
    val postsWriter: PostsWriter = PostsWriterImpl()

    val runner = Runner(postsFetcher, postsWriter)

    runner.run()
}