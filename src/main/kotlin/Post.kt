import kotlinx.serialization.*

@Serializable
data class Post(
    val id: Long,
    val userId: Long,
    val title: String,
    val body: String,
)
