import fetcher.PostsFetcher
import writer.PostsWriter

class Runner(private val postsFetcher: PostsFetcher, private val postsWriter: PostsWriter) {

    suspend fun run() {
        val fetchedPosts = postsFetcher.fetch()
        postsWriter.write(fetchedPosts)
    }
}