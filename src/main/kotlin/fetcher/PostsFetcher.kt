package fetcher

import Post
import kotlinx.coroutines.flow.Flow

interface PostsFetcher {
    fun fetch(): Flow<Post>
}