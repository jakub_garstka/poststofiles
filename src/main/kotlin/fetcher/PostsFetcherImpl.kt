package fetcher

import Post
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okhttp3.*
import java.io.IOException

const val EMPTY_ARRAY_AS_STRING = "[]"
const val POSTS_URL = "https://jsonplaceholder.typicode.com/posts";

class PostsFetcherImpl(private val client: OkHttpClient = OkHttpClient()): PostsFetcher {

    override fun fetch(): Flow<Post> {
        val request = buildRequest()
        return flow {
            val postsList = makeACall(request)
            postsList.forEach { emit(it) }
        }
    }

    private fun makeACall(request: Request): List<Post> {
        return try {
            val response = client.newCall(request).execute()
            val bodyAsString = getResponseBodyAsString(response)
            mapBodyToPosts(bodyAsString)
        } catch (e: IOException) {
            e.printStackTrace()
            emptyList()
        }
    }

    private fun buildRequest(): Request =
        Request.Builder().url(POSTS_URL).build()

    private fun getResponseBodyAsString(response: Response): String =
        response.body?.string() ?: EMPTY_ARRAY_AS_STRING

    private fun mapBodyToPosts(bodyAsString: String): List<Post> =
        Json.decodeFromString(bodyAsString)

}