package writer

import Post
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

const val FILE_EXTENSION = "json"
const val DIRECTORY = "posts"

class PostsWriterImpl : PostsWriter {
    override suspend fun write(postsFlow: Flow<Post>) {
        postsFlow.collect { writePostToFile(it, it.id.toString()) }
    }

    private fun preparePostToWriting(post: Post): String = Json.encodeToString(post)

    private fun writePostToFile(post: Post, fileName: String) {
        val postAsString = preparePostToWriting(post);
        File("${DIRECTORY}/${fileName}.${FILE_EXTENSION}").writeText(postAsString)
    }
}