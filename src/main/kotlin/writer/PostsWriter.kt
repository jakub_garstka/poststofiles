package writer

import Post
import kotlinx.coroutines.flow.Flow

interface PostsWriter {
    suspend fun write(postsFlow: Flow<Post>)
}
