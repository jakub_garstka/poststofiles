package fetcher

import Post
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.io.IOException

val POST_1 = Post(id = 1, userId = 1, title = "1", body = "1")
val POST_2 = Post(id = 2, userId = 2, title = "2", body = "2")

val POSTS = listOf(POST_1, POST_2)

const val RESPONSE_MESSAGE = "OK"
const val RESPONSE_CODE = 200
val RESPONSE_BODY = Json.encodeToString(POSTS);
val RESPONSE_MEDIA_TYPE = "application/json".toMediaTypeOrNull()

internal class PostsFetcherImplTest {

    private fun buildResponseWithPosts(): Response =
        Response.Builder()
            .request(Request.Builder().url(POSTS_URL).build())
            .protocol(Protocol.HTTP_2)
            .message(RESPONSE_MESSAGE)
            .code(RESPONSE_CODE)
            .body(RESPONSE_BODY.toResponseBody(RESPONSE_MEDIA_TYPE))
            .build()

    private fun buildResponseWithoutPosts(): Response =
        Response.Builder()
            .request(Request.Builder().url(POSTS_URL).build())
            .protocol(Protocol.HTTP_2)
            .message(RESPONSE_MESSAGE)
            .code(RESPONSE_CODE)
            .build()


    @Test
    fun `fetch two posts and check if those structure and content are equal to expected`() = runBlocking {
        val response = buildResponseWithPosts()

        val callMock = mock<Call> {
            on { execute() }.doReturn(response)
        }

        val clientMock = mock<OkHttpClient> {
            on { newCall(any()) }.doReturn(callMock)
        }

        val postsFetcher = PostsFetcherImpl(clientMock)
        val fetchedPost = postsFetcher.fetch().toList()

        assertEquals(POSTS, fetchedPost)
    }

    @Test
    fun `try to fetch posts but some IO exception occurred`() = runBlocking {

        val callMock = mock<Call> {
            on { execute() }.doThrow(IOException())
        }

        val clientMock = mock<OkHttpClient> {
            on { newCall(any()) }.doReturn(callMock)
        }

        val postsFetcher = PostsFetcherImpl(clientMock)
        val fetchedPost = postsFetcher.fetch().toList()
        val emptyList = emptyList<Post>()

        assertEquals(emptyList, fetchedPost)
    }

    @Test
    fun `do fetch, no data come`() = runBlocking {
        val response = buildResponseWithoutPosts()

        val callMock = mock<Call> {
            on { execute() }.doReturn(response)
        }

        val clientMock = mock<OkHttpClient> {
            on { newCall(any()) }.doReturn(callMock)
        }

        val postsFetcher = PostsFetcherImpl(clientMock)
        val fetchedPost = postsFetcher.fetch().toList()
        val emptyList = emptyList<Post>()

        assertEquals(emptyList, fetchedPost)
    }

}