package writer

import Post
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertTrue

import java.io.File
import java.io.IOException

const val NOT_DIRECTORY = "Provided path does not point to directory"

const val NOT_SAME_NAMES = "Created files have no expected names"
const val NOT_SAME_CONTENT = "Created files have no expected content"
const val NOT_EMPTY_DIRECTORY = "Directory is not empty"

val POST_1 = Post(id = 1, userId = 1, title = "1", body = "1")
val POST_2 = Post(id = 2, userId = 2, title = "2", body = "2")

val POSTS = listOf(POST_1, POST_2)
val POSTS_FLOW = flow { POSTS.forEach { emit(it) } }
val EMPTY_POSTS_FLOW = emptyFlow<Post>()

internal class PostsWriterImplTest {

    private fun assertIfFilesHaveExpectedNames(expectedFilesNames: List<String>) {
        val directory = getDirectory()
        val filesNames = directory.listFiles()!!.map { it.name }
        val areFilesNamesSameAsExpected = checkIfListOfArraysAreTheSame(filesNames, expectedFilesNames)
        assertTrue(areFilesNamesSameAsExpected, NOT_SAME_NAMES);

    }

    private fun assertIfFilesHaveExpectedContent(expectedFilesContent: List<String>) {
        val directory = getDirectory()
        val filesContent = directory.listFiles()!!.map { it.readText() }
        val isFilesContentSameAsExpected = checkIfListOfArraysAreTheSame(filesContent, expectedFilesContent)
        assertTrue(isFilesContentSameAsExpected, NOT_SAME_CONTENT);

    }

    private fun assertIfDirectoryIsEmpty() {
        val directory = getDirectory()
        val isDirectoryEmpty = directory.listFiles()!!.isEmpty()
        assertTrue(isDirectoryEmpty, NOT_EMPTY_DIRECTORY)


    }

    private fun getDirectory(): File {
        val possibleDirectory = File(DIRECTORY)
        if (possibleDirectory.isDirectory) {
            return possibleDirectory
        } else
            throw IOException(NOT_DIRECTORY)
    }

    private fun checkIfListOfArraysAreTheSame(firstList: List<String>?, secondList: List<String>?): Boolean {
        return if (firstList == null && secondList == null) {
            true
        } else if (firstList != null && secondList == null || firstList == null && secondList != null) {
            false
        } else {
            firstList!!.size == secondList!!.size && firstList.containsAll(secondList) && secondList.containsAll(
                firstList
            )
        }

    }

    private fun prepareExpectedFilesNames(postsIds: List<Long>): List<String> =
        postsIds.map { "${it}.${FILE_EXTENSION}" }

    private fun prepareExpectedFilesContent(posts: List<Post>): List<String> =
        posts.map { Json.encodeToString(it) }


    @BeforeEach
    @AfterEach
    fun cleanUp() {
        val directory = File(DIRECTORY)
        if (directory.isDirectory) {
            directory.listFiles()?.forEach { it.delete() }
        }
    }

    @Test
    fun `write two posts in separate files and check if files have expected names`() =
        run {
            val expectedFilesNames = prepareExpectedFilesNames(POSTS.map { it.id })
            val postsWriter = PostsWriterImpl()
            runBlocking {
                postsWriter.write(POSTS_FLOW)
            }
            assertIfFilesHaveExpectedNames(expectedFilesNames)
        }

    @Test
    fun `write two posts in separate files and check if files have expected content`() = run {
        val expectedFilesContent = prepareExpectedFilesContent(POSTS)
        val postsWriter = PostsWriterImpl()
        runBlocking {
            postsWriter.write(POSTS_FLOW)
        }
        assertIfFilesHaveExpectedContent(expectedFilesContent)
    }

    @Test
    fun `write no posts and check if directory is empty`() = run {
        val emptyPostsList = emptyList<Post>()
        val postsWriter = PostsWriterImpl()
        runBlocking {
            postsWriter.write(EMPTY_POSTS_FLOW)
        }
        assertIfDirectoryIsEmpty()
    }


}